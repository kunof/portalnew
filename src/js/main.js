/*
 * Third party
 */
//= ../../bower_components/modernizr/modernizr.js
//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/uikit/js/uikit.js


/*
 * Custom
 */
//= partials/app.js
//= partials/jquery.fancybox.js
//= partials/owl.carousel.js
//= partials/phonemask.js
//= partials/goodshare.js
//= partials/jquery.selectric.js
//= partials/dropezone.js
//= partials/jquery.imgareaselect.pack.js
//= partials/datetimepicker.js


$(function(){
  Dropzone.options.myAwesomeDropzone = {
    maxFilesize: 5,
    addRemoveLinks: true,
    dictResponseError: 'Server not Configured',
    acceptedFiles: ".png,.jpg,.gif,.bmp,.jpeg",
    init:function(){
      var self = this;
      // config
      self.options.addRemoveLinks = true;
      self.options.dictRemoveFile = "Delete";
      //New file added
      self.on("addedfile", function (file) {
        console.log('new file added ', file);
      });
      // Send file starts
      self.on("sending", function (file) {
        console.log('upload started', file);
        $('.meter').show();
      });
      
      // File upload Progress
      self.on("totaluploadprogress", function (progress) {
        console.log("progress ", progress);
        $('.roller').width(progress + '%');
      });

      self.on("queuecomplete", function (progress) {
        $('.meter').delay(999).slideUp(999);
      });
      
      // On removing file
      self.on("removedfile", function (file) {
        console.log(file);
      });
    }
  };
})

$('.datepick').datepicker({
    'format': 'd-m-yyyy',
    'autoclose': true
});
//$('.developments-events .dots-button').on('click', function(){
//    $('.developments-events .datepick').datepicker('show');
//});
$('header .city').click(function(){
    $(this).toggleClass('active');
    $('.city-select').toggleClass('active');
});
$('.close-cityes').click(function(){
    $('header .city').toggleClass('active');
    $('.city-select').toggleClass('active');
});
$('.open-modal').fancybox();
$('.close-modal').click(function(){
    $.fancybox.close(); 
});
$('header .account.user-login, .filter>div').click(function(){
    $('header').removeClass('active');
});
$('.button-hamburger').click(function(){
    $('header').toggleClass('active');
});
$('.mobile-overlay').click(function(){
    $('header').toggleClass('active');
});
$('.image-block .user-icon').click(function(){
    var windowWidth = $(window).width();
    if (windowWidth<=640){}else{
        $(this).parents('.image-block').find('.user-box-text').addClass('active').animate({marginTop:0},100,function(){
            $(this).focus();
        });
        return false;
    }
});
$('.user-box-text').focusout(function(){
    $(this).removeClass('active');
});
$('.user-box-text .close-user-box').click(function(){
    $(this).parents('.user-box-text').removeClass('active');
});
$('.page-scroll-down').click(function(){
    var heightW = $('.main-image').height();
    var winWidth = $(window).width();
    if (winWidth<= 1300){
        heightW = heightW - 52;
    }
    $('body').animate({
            scrollTop: heightW
        }, 700);
    return false;
});
$('.banner-slider .slider').owlCarousel({
    items:1,
    loop:true,
    nav:true,
    controls:true
});
$('.orientation-buttons>div').click(function(){
    $(this).removeClass('none-active');
    $(this).siblings().addClass('none-active');
    if ($(this).hasClass('horizontal-button')){
        $('.controls-page>.content-inst-element').addClass('horizontal-orientation-raklama');
    }else{
        $('.controls-page>.content-inst-element').removeClass('horizontal-orientation-raklama');
    }
});
$('.buttons-switch>*').click(function(){
    $(this).parent().find('div').removeClass('active');
    $(this).addClass('active');
     var id = $(this).data('href');
    $('.swithes-block.active').slideUp();
    $('.swithes-block').removeClass('active');
    $(id).slideDown().addClass('active');
});
$('.events-carousel').owlCarousel({
    items:5,
    loop:true,
    nav:true,
    margin:12,
    controls:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
        400:{
            items:2,
        },
        600:{
            items:3,
        },
        800:{
            items:4,
        },
        1000:{
            items:5,
        }
    }
});
$('.menu-slider').owlCarousel({
    items:1,
    controls:true,
    loop:false,
    nav:true
});
$('.scrolling-nav a').click(function(){
    var id = $(this).attr('href');
    var position = $(id).offset();
    var windowSize = $(window).width();
    var scrollTo = position.top;
    if (windowSize<=1300){
        scrollTo = scrollTo - 52;
    }
    $('body').animate({
        scrollTop: scrollTo
    }, 700); 
    return false;
});
$('.scrooll-to').click(function(){
    var id = $(this).attr('href');
    var position = $(id).offset();
    $('body').animate({
        scrollTop: position.top
    }, 700);
    return false;
});
$( "body" ).on( "click", ".set-stars .stars div", function() {
    $(this).parent().find('div').removeClass('active');
    var index = $(this).index();
    $(this).parent().siblings('.rating-value').val(index+1);
    for (var i = 0; i<=index; i++){
        $(this).parent().find('div').eq(i).addClass('active');
    }
});

$('#userphoto.add-photo-area input[type="file"]').change(function(){
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        
    }else{
        $('#next-step-photo').trigger('click');
        setTimeout(function(){
            var x1 = 20;
            var y1 = 0;
            var x2 = 80;
            var y2 = 100;
            var imgw = $('#upload-photo-user-miniature .image-area-miniature img').width();
            var imgh = $('#upload-photo-user-miniature .image-area-miniature img').height();
            var perW = imgw / 100;
            var perH = imgh / 100;
            x1 = x1*perW;
            x2 = x2*perW;
            y1 = y1*perH;
            y2 = y2*perH;
            $('#upload-photo-user-miniature .image-area-miniature img').imgAreaSelect({
                x1: x1, y1: y1, x2: x2, y2: y2, handles: true, aspectRatio: '1:1', minHeight:50, minWidth:50, parent:$('#upload-photo-user-miniature')
            });
        },100);
    }
    //readURL(this);
});
$('.photos-more').click(function(){
    var id = $(this).data('moreid');
    $('#'+id).fadeIn();
    $('body').addClass('overflow-hidden');
    return false;
});
$('.gallery-photos-more .close').click(function(){
   $('.gallery-photos-more').fadeOut();
    $('body').removeClass('overflow-hidden');
});
//var myDropzone = Dropzone.forElement("#my-awesome-dropzone");
//myDropzone.on("error", function(file, message) { alert(message); });
$('.open-modal-miniature').fancybox({
    beforeClose : function(){
        $('#upload-photo-user-miniature .image-area-miniature img').imgAreaSelect({
            remove:true,
            instance: true
        });
    }
});
$("[data-fancybox]").fancybox({
	beforeClose : function(){
        $('#upload-photo-user-miniature .image-area-miniature img').imgAreaSelect({
            remove:true,
            instance: true
        });
    }
});

$('.account.user-login').click(function(){
    if ($(this).hasClass('active')){
        $(this).removeClass('active');
    }else{
        $(this).addClass('active');
        $(this).addClass('no-target');
        setTimeout(function(){
            $('.account.user-login').removeClass('no-target');
        },300);
    }
});
$("*").click(function(){
    if ($('.account.user-login').hasClass('no-target')){
        
    }else{
        $('.account.user-login').removeClass('active');
    }
});
$('.open-modal-miniature-edit').fancybox({
    onComplete  : function(){
        var x1 = 0;
        var y1 = 0;
        var x2 = 100;
        var y2 = 100;
        var imgw = $('#edit-user-miniature .image-area-miniature img').width();
        var imgh = $('#edit-user-miniature .image-area-miniature img').height();
        var perW = imgw / 100;
        var perH = imgh / 100;
        x1 = x1*perW;
        x2 = x2*perW;
        y1 = y1*perH;
        y2 = y2*perH;
        $('#edit-user-miniature .image-area-miniature img').imgAreaSelect({
            x1: x1, y1: y1, x2: x2, y2: y2, handles: true, aspectRatio: '1:1', minHeight:50, minWidth:50, parent:$('#edit-user-miniature'),
            onInit: function (img, selection) {
                $('.imgareaselect-border4').addClass('circle');
            }
        });
    },
    beforeClose : function(){
        $('#edit-user-miniature .image-area-miniature img').imgAreaSelect({
            remove:true,
            instance: true
        });
    }
});
//Загрузка изображения
    //function readURL(input) {
    //    if (input.files && input.files[0]) {
    //        var reader = new FileReader();
    //        reader.onload = function (e) {
    //            $('#upload-photo-user-miniature .image-area-miniature img').attr('src', e.target.result);
    //        }
    //        reader.readAsDataURL(input.files[0]);
    //    }
    //}
$('.user-list-control .user-list-item .remove-user').click(function(){
    $(this).parents('.user-list-item').remove();
    return false;
});
$('.dates-events-tab').hide();
$('.filter-button').click(function(){
    var dataMolalFilter = $(this).data('id');
    if (dataMolalFilter != null){
        $('#' + dataMolalFilter).addClass('active');
    }else{
        $('.filter-modal').addClass('active');
    }
});
$('.lc-navigation a').click(function(){
    if ($(this).hasClass('active')){}else{
        $('.lc-navigation a').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('href');
        $('.lc-tab, .lc-page .bg-left-col').fadeOut(150,function(){
            setTimeout(function(){$(id).fadeIn(150);},150);
            $('.lc-page .bg-left-col').fadeIn();
        });
    }
    return false;
});
$('.close-filter, .filter-overlay').click(function(){
    $('.filter-modal').removeClass('active');
});
$('.instit-item').mouseenter(function(){
    $(this).find('.params').slideDown(200);
});
$('.instit-item').mouseleave(function(){
    $(this).find('.params').slideUp(200);
});
$( "body" ).on( "click", ".main-image-upload", function() {
    $(this).parents(".dropzone").find(".dz-preview").removeClass('itsMine');
    $(this).parents(".dz-preview").addClass('itsMine'); 
});
//$( "body" ).on( "click", ".open-view-image", function() {
//    $('.view-image-modal').addClass('open');
//    $('.view-image-modal .loader').fadeIn();
//    $('body').addClass('overflow-hidden');
//    var src = $(this).data('url');
//    $('.view-image-modal .download-image').attr('href',src);
//    $('.view-image-modal .image-container img').attr('src',src);
//    $(this).parents('.image-block').addClass('active');
//    $('.view-image-modal .image-container img').load(function() {
//        $(this).fadeIn();
//        $('.view-image-modal .loader').fadeOut();
//        $('.bottom-absolute-container').addClass('active');
//    });
//    return false;
//});
$( "body" ).on( "click", ".open-view-image", function() {
    var id = $(this).data('id');
    $('.view-image-modal').addClass('open');
    $('.view-image-modal .loader').fadeIn();
    $('body').addClass('overflow-hidden');
    var src = $('#'+id).data('url');
    $('.view-image-modal .download-image').attr('href',src);
    $('.view-image-modal .image-container img').attr('src',src);
    $('#'+id).addClass('active');
    $('.view-image-modal .image-container img').load(function() {
        $(this).fadeIn();
        $('.view-image-modal .loader').fadeOut();
        $('.bottom-absolute-container').addClass('active');
    });
    return false;
});



$( "body" ).on( "click", ".removeDropImage", function() {
    //$(this).parents('.dz-preview').remove();
});
$('.view-image-modal .image-container').click(function(){
    closeImageView();
});
//function closeImageView(){
//    if ($('.view-image-modal').hasClass('people-label-active')){
//        
//    }else{
//        $('.image-block').removeClass('active');
//        $('.view-image-modal').removeClass('open');
//        $('body').removeClass('overflow-hidden');
//        $('.view-image-modal .image-container img').attr('src','');
//        $('.view-image-modal .image-container img').hide();
//        $('.view-image-modal .download-image').attr('href','');
//        $('.bottom-absolute-container').removeClass('active');
//    }
//}
function closeImageView(){
    if ($('.view-image-modal').hasClass('people-label-active')){
        
    }else{
        $('.view-images-block-list div').removeClass('active');
        $('.view-image-modal').removeClass('open');
        $('body').removeClass('overflow-hidden');
        $('.view-image-modal .image-container img').attr('src','');
        $('.view-image-modal .image-container img').hide();
        $('.view-image-modal .download-image').attr('href','');
        $('.bottom-absolute-container').removeClass('active');
    }
}
function nextImageView(){
    if ($('.view-images-block-list div').last().hasClass('active')){
        var nowElement = $('.view-images-block-list div').first();
        nowElement.addClass('active');
        $('.view-images-block-list div').last().removeClass('active');
        var src = nowElement.data('url');
    }else{
        var nowElement = $('.view-images-block-list .active').next();
        nowElement.addClass('active').prev().removeClass('active');
        var src = nowElement.data('url');
    }
    $('.view-image-modal .loader').fadeIn();
    $('.view-image-modal .image-container img').fadeOut(300,function(){
        $('.view-image-modal .image-container img').attr('src',src);
        $('.view-image-modal .image-container img').load(function() {
            $(this).fadeIn();
            $('.view-image-modal .loader').fadeOut();
        });
    });
}

function prevImageView(){
    if ($('.view-images-block-list div').first().hasClass('active')){
        var nowElement = $('.view-images-block-list div').last();
        nowElement.addClass('active');
        $('.view-images-block-list div').first().removeClass('active');
        var src = nowElement.data('url');
    }else{
        var nowElement = $('.view-images-block-list .active').prev();
        nowElement.addClass('active').next().removeClass('active');
        var src = nowElement.data('url');
    }
    $('.view-image-modal .loader').fadeIn();
    $('.view-image-modal .image-container img').fadeOut(300,function(){
        $('.view-image-modal .image-container img').attr('src',src);
        $('.view-image-modal .image-container img').load(function() {
            $(this).fadeIn();
            $('.view-image-modal .loader').fadeOut();
        });
    });
}
$('body').on('keyup',function(e){
      if(e.which==27){
          //Enter
          if ($('.view-image-modal').length){
              if ($('.view-image-modal').hasClass('open')){
                  closeImageView();
              }
          }
      }else if (e.which==39){
          //Right
          if ($('.view-image-modal').length){
              if ($('.view-image-modal').hasClass('open')){
                  nextImageView();
              }
          }
      }else if (e.which==37){
          //Left
          if ($('.view-image-modal').length){
              if ($('.view-image-modal').hasClass('open')){
                  prevImageView();
              }
          }
      }
    });
$('.bottom-absolute-container .owl-next').click(function(){
    nextImageView();
});
$('.bottom-absolute-container .owl-prev').click(function(){
     prevImageView();
});
var arrayCoordinats = [];
var arrayOne = [];
function coordinats(x1, y1, x2, y2){
    var imgw = $('.view-image-modal .image-container img').width();
    var imgh = $('.view-image-modal .image-container img').height();
    var perW = imgw / 100;
    var perH = imgh / 100;
    x1 = x1/perW;
    x2 = x2/perW;
    y1 = y1/perH;
    y2 = y2/perH;
    arrayOne = [x1, y1, x2, y2];
}
$('.box-searches-users .result-item').click(function(){
    var name = $(this).data('username');
    var id = $(this).data('userid');
    $('.people-label-close').trigger('click');
    arrayOne.push(name,id);
    arrayCoordinats.push(arrayOne);
    console.log(arrayCoordinats);
    var html = '';
    for (var i=0; i<arrayCoordinats.length; i++){
        html += '<span class="nameLi" data-x1="'+arrayCoordinats[i][0]+'" data-y1="'+arrayCoordinats[i][1]+'" data-x2="'+arrayCoordinats[i][2]+'"  data-y2="'+arrayCoordinats[i][3]+'" data-userid="'+arrayCoordinats[i][5]+'"><span class="name">'+arrayCoordinats[i][4]+'</span><i class="removeUser"></i></span>';
    }
    $('.people-lables').html(
        '<span>На этой фотографии:</span>'+ html
    );
});

$('.view-image-modal .people-lable').click(function(){
    if ($(this).hasClass('active')){
        closePeopleLabel();
    }else{
        $('.view-image-modal .container-content.viewers-container .socials, .people-lables').fadeOut();
        $(this).parents('.view-image-modal').addClass('people-label-active');
        
        //http://odyniec.net/projects/imgareaselect/usage.html 
        $('.view-image-modal .image-container img').imgAreaSelect({
            handles: true,
            minHeight: 50,
            minWidth: 50,
            onSelectEnd: function (img, selection) {
                coordinats(selection.x1, selection.y1, selection.x2, selection.y2);
                $('.peoplelink-container').fadeIn();
            }
        });
    }
    $(this).toggleClass('active');
});
$( "body" ).on( "click", ".people-lables .nameLi .removeUser", function() {
    var id = $(this).parents('.nameLi').data('userid');
    for (var i=0; i<arrayCoordinats.length; i++){
        if (arrayCoordinats[i][5] == id){
            arrayCoordinats.splice(i,1);
        }
    }
    $('.view-image-modal .image-container img').imgAreaSelect({
        remove:true,
        instance: true
    });
    $(this).parents('.nameLi').remove();
    if ($('.people-lables').find('.nameLi').length <1){
        $('.people-lables').html(' ');
    }
});
$( "body" ).on( "mouseover", ".people-lables .nameLi", function() {
    var x1 = $(this).data('x1');
    var y1 = $(this).data('y1');
    var x2 = $(this).data('x2');
    var y2 = $(this).data('y2');
    var imgw = $('.view-image-modal .image-container img').width();
    var imgh = $('.view-image-modal .image-container img').height();
    var perW = imgw / 100;
    var perH = imgh / 100;
    x1 = x1*perW;
    x2 = x2*perW;
    y1 = y1*perH;
    y2 = y2*perH;
    $('.view-image-modal .image-container img').imgAreaSelect({
        x1: x1, y1: y1, x2: x2, y2: y2, handles: true
    });
});
$( "body" ).on( "mouseout", ".people-lables .nameLi", function() {
    $('.view-image-modal .image-container img').imgAreaSelect({
        remove:true,
        instance: true
    });
});
$('.people-label-close').click(function(){
    closePeopleLabel();
    $('.view-image-modal .people-lable').removeClass('active');
    $('.view-image-modal .image-container img').imgAreaSelect({
        remove:true,
        instance: true,
        onSelectEnd: function (img, selection) {
            $('.peoplelink-container').fadeOut();
        }
    });
});
function closePeopleLabel(){
    $('.view-image-modal').removeClass('people-label-active');
    $('.view-image-modal .container-content.viewers-container .socials, .people-lables').fadeIn();
    
    $('.peoplelink-container').fadeOut();
    $('.view-image-modal .image-container img').imgAreaSelect({
        handles: false
    });
}
$('.slideToBlock').click(function(){
    var id = $(this).attr('href');
    $(this).parents('.slidedBlock').slideUp(300, function(){
        $(id).slideDown(300);
    });
});
$('.selectric').selectric();
$('.input.error input').focusin(function(){
    $(this).val('');
    $(this).parents('.input').removeClass('error');
});

function mask(){
    $(".phone-input").mask("+7(999)-999-99-99");
}
mask();
$('.time-input').mask("99:99");
$('.duplicate-block-input').click(function(){
    var id = $(this).attr('href');
    var html = $(id).html();
    $(this).parents('.input').before('<div class="input gray">'+html+'</div>');
    mask();
    return false;
});
$('.edit-page-buttons>div').click(function(){
    var idModal = $(this).data('editblock');
    $(this).parents('.modal-edit-gallery-content').hide();
    $('.edit-menu-modals').show();
    $('#'+idModal).show();
});
$('.edit-menu-modals .close-this').click(function(){
    $('.modal-edit-gallery-content').show();
    $('.edit-menu-modals>div, .edit-menu-modals').hide();
});
$('.password-write').keyup(function(){
    var value = $(this).val();
    checkPassword(value);
});
$('.likes').click(function(){
    var val = $(this).find('span').text(); var val = parseFloat(val);
    if ($(this).hasClass('active')){
        $(this).find('span').text(val-1);
    }else{
        $(this).find('span').text(val+1);
    }
    $(this).toggleClass('active');
});
$('.controls-button-gallery td').click(function(){
    $(this).parents('.controls-button-gallery').find('td').removeClass('active');
    $(this).addClass('active');
});
$('.select-colors>div').click(function(){
    $(this).parents('.select-colors').find('div').removeClass('active');
    $(this).addClass('active');
});
function checkPassword(value) {
    var password = value;
    var s_letters = "qwertyuiopasdfghjklzxcvbnmйцукенгшщзхъфывапролджэячсмитьбю"; // Буквы в нижнем регистре
    var b_letters = "QWERTYUIOPLKJHGFDSAZXCVBNMЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ"; // Буквы в верхнем регистре
    var digits = "0123456789"; // Цифры
    var specials = "!@#$%^&*()_-+=\|/.,:;[]{}"; // Спецсимволы
    var is_s = false; // Есть ли в пароле буквы в нижнем регистре
    var is_b = false; // Есть ли в пароле буквы в верхнем регистре
    var is_d = false; // Есть ли в пароле цифры
    var is_sp = false; // Есть ли в пароле спецсимволы
    for (var i = 0; i < password.length; i++) {
      /* Проверяем каждый символ пароля на принадлежность к тому или иному типу */
      if (!is_s && s_letters.indexOf(password[i]) != -1) is_s = true;
      else if (!is_b && b_letters.indexOf(password[i]) != -1) is_b = true;
      else if (!is_d && digits.indexOf(password[i]) != -1) is_d = true;
      else if (!is_sp && specials.indexOf(password[i]) != -1) is_sp = true;
    }
    if (password.length < 1){
        $('.password-label').removeClass('active');
    }else{
        $('.password-label').addClass('active');
    }
    var rating = 0;
    var text = "";
    var percent = 0;
    if (is_s) rating++; // Если в пароле есть символы в нижнем регистре, то увеличиваем рейтинг сложности
    if (is_b) rating++; // Если в пароле есть символы в верхнем регистре, то увеличиваем рейтинг сложности
    if (is_d) rating++; // Если в пароле есть цифры, то увеличиваем рейтинг сложности
    if (is_sp) rating++; // Если в пароле есть спецсимволы, то увеличиваем рейтинг сложности
    /* Далее идёт анализ длины пароля и полученного рейтинга, и на основании этого готовится текстовое описание сложности пароля */
    if (password.length < 6 && rating < 3){text = "Простой"; percent=20;} 
    else if (password.length < 6 && rating >= 3){text = "Средний"; percent=66;} 
    else if (password.length >= 8 && rating < 3){text = "Средний"; percent=66;} 
    else if (password.length >= 8 && rating >= 3){text = "Сложный"; percent=100;} 
    else if (password.length >= 6 && rating == 1){text = "Простой"; percent=20;} 
    else if (password.length >= 6 && rating > 1 && rating < 4){text = "Средний"; percent=66;} 
    else if (password.length >= 6 && rating == 4){text = "Сложный"; percent=100;} 
    $('.password-label p span').html(text);
    $('.password-label .progress-line>div').css('width',percent+'%');
    if (percent > 50){
        $('.password-write').parents('.input').addClass('success');
    }else{
        $('.password-write').parents('.input').removeClass('success');
    } 
    $('.password-write').focusout(function(){
        $('.password-label').removeClass('active');
    });
    return false; // Форму не отправляем
  }



//Google
var iMap = 0;
var $viewInMap = $('.view-in-map');
var map;
var markers = [];
var infoWindows = [];

if ($viewInMap.length) {
	google.maps.event.addDomListener(window, 'load', init);
}
function init() {

    // Рендерим карту
    var mapOptions = {
        zoom: 10,
        scrollwheel: false,
        center: new google.maps.LatLng(60.603675842285156, 56.88422847947373),
    };
    var mapElement = document.getElementById('map-points');
    map = new google.maps.Map(mapElement, mapOptions);

    // Наносим метки офисов
   $('.view-in-map').each(function(index) {        
        var lat = parseFloat($(this).data('lat')),
            lng = parseFloat($(this).data('lng')),
            zoom = parseFloat($(this).data('zoom')),
            icon = $(this).data('icon'),
            content = $('.baloon-content>div').eq(iMap).html();
       if (icon== null){
           icon = "/img/baloon-map.png";
       }
        var pos = new google.maps.LatLng(lat, lng);
        var marker = new google.maps.Marker({
            map: map,
            position: pos,
            animation: google.maps.Animation.DROP,
            icon: icon,
        });
        var contentString = content;
        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        marker.addListener('click', function() {
            infowindow.open(map, marker);
        });

        markers[lat + '.' +  lng] = marker;
        infoWindows[lat + '.' +  lng] = infowindow;
        // Устанавливаем карту на первый адрес
        if (index == 0) {
            map.setCenter(pos);
            map.setZoom(zoom);
        }

       iMap ++;
    });

}